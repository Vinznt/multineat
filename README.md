# MultiNEAT

Instrucciones de instalacion en Kubuntu 18.04

1.- Descargar la librería, desde este repositorio o desde el oficial: https://github.com/peter-ch/MultiNEAT

NOTA: lo que instalemos aquí será a traves de los paquetes de Ubuntu, sin usar pip3. Esto lo hago así, por los problemas que creo que pude tener en su momento el instalarlos con pip3


2.- Instalar, como se indica en la "documentación" de la página web del multiNEAT (http://www.multineat.com/download.html) los "Python Headers" en Python3.
Ubuntu 18.04 viene con Python2.7 y Python 3.5 por defecto. Para la libreía, como se indica en la documentación, ha de usarse Python3.

sudo apt install build-essential python3-dev

Fuente: https://askubuntu.com/questions/398489/how-to-install-build-essential
Fuente: https://askubuntu.com/questions/524028/how-can-i-install-python-dev-off-apt-get


3.- Instalamos la librería de compilación de c++ "Boost":

sudo apt-get install libboost-all-dev
sudo apt-get install aptitude
aptitude search boost

Aptitude nos sirve para verificar los archivos que contienen la palabra boost
Fuente: https://stackoverflow.com/questions/12578499/how-to-install-boost-on-ubuntu



4.- Instalmos la herramienta "psutil" que la pide:
sudo apt-get install python3-psutil

Fuente: https://www.howtoinstall.co/es/ubuntu/xenial/python-psutil


 5.- Ejecutamos el "setup.py" dentro del directorio de MultiNEAT
 
sudo python3 setup.py install 

Y seguimos el paso de indicarle al operativo cuál es la variable de compilación de c++:

export MN_BUILD=boost


6.- Llegados a este punto, es posible que todavía dea el siguiente error:

Source file is missing and MN_BUILD environment variable is not set.
Specify either 'cython' or 'boost'. Example to build in Linux with Cython:
        $ export MN_BUILD=cython
        
Para ello, hay que hacer dos cosas:

    1.- Asegurarse de que boost esta correctamente instalado y es reconocido por el sistema. Esto se hace del siguiente modo:

    export MN_BUILD=boost (si no lo hemos hecho antes)

    ejecutamos python3 y nos aseguramos de que boost es reconocido por el sistema operativo:

        >>> python3
        >>> import os
        >>> print (os.getenv('MN_BUILD'))

    si retorna "boost" es que está instalado correctamente y listo para el paso 2.

    2.- Abrir el fichero de instalacion, el "setup.py" y debajo de la línea donde está escrtio "build_sys = os.getenv('MN_BUILD')"
    escribir:

    build_sys = 'boost'

    Por algún motivo, el "setup.py" no es capaz de trabajar bien con su propia variable "build_sys".

    
7.- Se guarda la modificación del fichero, y se vuelve a ejecutar el "setup.py". No debería de haber ningún problema en su instalación y la librería MultiNEAT debería de quedar instalada.


